Please clear changelog after each release.
Thank you!

-----------------
- Changed protocol version to 4.
- Reorganized `en_us.lang` and removed many unused strings.
- Tweaked the grammar of some config strings.
- Changed the internal names of some config options.
- Converted recipes and advancements to be automatically generated.
- Added recipes for crafting Stripped Wood/Hyphae from Hollowed Logs/Stems.
- Added many missing recipe unlocks.
- All Glory of the Snow variants now only yield one dye instead of two.
- The Null Block recipe now yield two Null Blocks instead of one.
- 1.20.4+: Renamed `wilderwild:potted_grass` to `wilderwild:potted_short_grass`.
- 1.20.1-1.20.4: Fixed a crash when interacting with Shelf Fungus.
- Optimized many textures for a smaller file size.
- Removed Quilt from supported mod loaders on Modrinth.
